﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace DailySQLRunner
{
    internal class Program
    {
        private static List<string> log;

        static Program()
        {
            Program.log = new List<string>();
        }

        public Program()
        {
        }

        [DllImport("kernel32", CharSet = CharSet.None, ExactSpelling = false)]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        public static string IniReadValue(string fileName, string Section, string Key)
        {
            StringBuilder stringBuilder = new StringBuilder(255);
            Program.GetPrivateProfileString(Section, Key, "", stringBuilder, 255, fileName);
            return stringBuilder.ToString();
        }

        private static void Main(string[] args)
        {
            SqlConnection sqlConnection = null;
            List<string> strs = new List<string>();
            try
            {
                try
                {
                    string str = Path.ChangeExtension(Assembly.GetExecutingAssembly().Location, "ini");
                    string str1 = Program.IniReadValue(str, "database", "host");
                    string str2 = Program.IniReadValue(str, "database", "database");
                    string str3 = Program.IniReadValue(str, "database", "user");
                    string str4 = Program.IniReadValue(str, "database", "password");

                    var queries = File.ReadAllLines(str);
                    var startToRead = false;
                    foreach (var query in queries)
                    {
                        if (!startToRead)
                        {
                            if (query.Equals("[sql]")) startToRead = true;
                            else continue;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(query)) continue;
                            strs.Add(query.Trim());
                        }
                    }
                    //for (int i = 0; i < 1000; i++)
                    //{
                    //	string empty = string.Empty;
                    //	empty = Program.IniReadValue(str, "sql", string.Concat("cmd", i.ToString()));
                    //	if (empty != string.Empty)
                    //	{
                    //		strs.Add(empty);
                    //	}
                    //}
                    Console.WriteLine("Config:");
                    Console.WriteLine(string.Concat("host=", str1));
                    Console.WriteLine(string.Concat("database=", str2));
                    Console.WriteLine(string.Concat("user=", str3));
                    Console.WriteLine();
                    string[] strArrays = new string[] { "data source=", str1, ";persist security info=False;initial catalog=", str2, ";user id=", str3, ";password=", str4, ";" };
                    sqlConnection = new SqlConnection(string.Concat(strArrays));
                    sqlConnection.Open();
                    foreach (string str5 in strs)
                    {
                        string[] strArrays1 = str5.Split(new char[] { '|' });
                        if ((int)strArrays1.Length <= 1)
                        {
                            continue;
                        }
                        Program.RunSql(sqlConnection, strArrays1[0], strArrays1[1]);
                    }
                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(string.Concat(exception.ToString(), Environment.NewLine, exception.StackTrace));
                    Console.ResetColor();
                }
            }
            finally
            {
                sqlConnection.Close();
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Update completed. Cmd count = {0}", strs.Count);
            Console.ResetColor();
        }

        private static void RunSql(SqlConnection openedConnection, string sql, string output)
        {
            Console.WriteLine(string.Concat("RunSql begin: sql = ", sql, ", output = ", output));
            try
            {
                string directoryName = Path.GetDirectoryName(output);
                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }
                if (File.Exists(output))
                {
                    string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(output);
                    if (directoryName == "")
                    {
                        directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    }
                    string str = string.Concat(directoryName, "\\config_backup");
                    if (!Directory.Exists(str))
                    {
                        Directory.CreateDirectory(str);
                    }
                    string[] strArrays = new string[] { str, "\\", fileNameWithoutExtension, null, null };
                    strArrays[3] = DateTime.Now.ToString("_yyyyMMdd_HHmmss");
                    strArrays[4] = ".txt";
                    string str1 = string.Concat(strArrays);
                    File.Copy(output, str1);
                    Console.WriteLine(string.Concat("backup ", output, " to ", str1));
                }
                using (SqlCommand sqlCommand = new SqlCommand(sql, openedConnection))
                {
                    sqlCommand.CommandType = CommandType.Text;
                    DataTable dataTable = new DataTable();
                    (new SqlDataAdapter(sqlCommand)).Fill(dataTable);
                    List<string> strs = new List<string>();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        strs.Add(row[0].ToString());
                    }
                    if (strs.Count > 0)
                    {
                        File.WriteAllLines(output, strs.ToArray());
                    }
                }
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Concat(exception.ToString(), Environment.NewLine, exception.StackTrace));
                Console.ResetColor();
            }
            Console.WriteLine("RunSql end.");
            Console.WriteLine("=======================================");
        }
    }
}
