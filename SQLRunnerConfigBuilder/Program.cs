﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SQLRunnerConfigBuilder
{
    class Program
    {
        #region Properties
        public static string OutputFileName { get; set; }
        public static string VTSConfigPath { get; set; }
        public static string DBHost { get; set; }
        public static string DBName { get; set; }
        public static string DBUser { get; set; }
        public static string DBPassword { get; set; }
        public static List<string> Products { get; set; }
        public static List<string> Tasks { get; set; }
        #endregion

        static void Main(string[] args)
        {
            try
            {
                Start();
                Console.WriteLine(OutputFileName + " generated, press any keys to close the program.");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine("Press any keys to close the program.");
            }
            Console.ReadKey();
        }

        static void Start()
        {
            var iniPath = Path.ChangeExtension(Assembly.GetExecutingAssembly().Location, "ini");
            var lines = File.ReadAllLines(iniPath);
            var readConfig = false;
            foreach (var line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;
                if (readConfig)
                {
                    Tasks.Add(line.Trim());
                    continue;
                }
                var array = line.Split('=');
                switch (array[0].ToLower())
                {
                    case "output":
                        OutputFileName = array[1];
                        break;
                    case "vtsconfigpath":
                        VTSConfigPath = array[1];
                        break;
                    case "host":
                        DBHost = array[1];
                        break;
                    case "database":
                        DBName = array[1];
                        break;
                    case "user":
                        DBUser = array[1];
                        break;
                    case "password":
                        DBPassword = array[1];
                        break;
                    case "product":
                        Products = new List<string>();
                        Products.AddRange(array[1].Split(' '));
                        break;
                    case "tasks":
                        Tasks = new List<string>();
                        readConfig = true;
                        break;
                }
            }

            var sb = new StringBuilder();
            sb.AppendLine("[database]");
            sb.AppendLine("host=" + DBHost);
            sb.AppendLine("database=" + DBName);
            sb.AppendLine("user=" + DBUser);
            sb.AppendLine("password=" + DBPassword);
            sb.AppendLine(Environment.NewLine);
            sb.AppendLine("[sql]");

            foreach (var task in Tasks)
            {
                var array = task.Split(',');
                var spName = array[0];
                var outputPath = array[1];
                var spType = array[2];
                // sp_getholidayex_European 'CL'|C:\VTS\Config\Holidays\CL.txt
                if (spType.ToLower() == "single")
                {
                    var fullPath = Path.Combine(VTSConfigPath, outputPath);
                    sb.AppendLine(spName + "|" + fullPath);
                }
                else
                {
                    foreach (var product in Products)
                    {
                        var fullPath = Path.Combine(Path.Combine(VTSConfigPath, outputPath), product + ".txt");
                        if (spName.ToLower() == "sp_transfervtsrollinfo")
                        {
                            fullPath = Path.Combine(Path.Combine(VTSConfigPath, outputPath), product + "-roll.txt");
                        }
                        sb.AppendLine(string.Format("{0} '{1}'|{2}", spName, product, fullPath));
                    }
                }
            }

            File.WriteAllText(OutputFileName, sb.ToString());
        }
    }
}
